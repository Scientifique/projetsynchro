#include <cstdlib>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
        
using namespace std;

int main(int argc, char** argv) {

    int sema; /* sémaphore identification */
    key_t clef = 404; /* clef IPC */
    int nb_sema = 4; /* nombre de sémaphores à créer */
    int ret; /* code de retour de system call */
    int init_value = 1; /* valeur initiale du cpteur */
    
    printf("--> Creation du semaphore \n");
    //printf("CLEF_IPC=%d -- NB_ELEMENT=%d\n", clef, nb_sema);
    sema = semget(clef, nb_sema, IPC_CREAT|IPC_EXCL|S_IRUSR|S_IWUSR);
    if (sema == -1) {
        perror("SEMGET");
        exit(1);
    }
    
    printf("--> Initialisation du semaphore \n");
    //printf("CLEF_IPC=%d -- RANG=%d -- VALEUR=%d\n", clef, nb_sema, init_value);
    
    sema = semget(clef, 0, 0);
    if (sema == -1) {
        perror("ACQ_SEMA");
        exit(2);
    }
    
	/* initialisation du compteur */
    for(int i=0; i<nb_sema; i++){
		ret=semctl(sema, i, SETVAL, init_value);
		if (ret == -1) {
			perror("INIT_SEMA");
			exit(3);
		}
	}
    
    return 0;
}

