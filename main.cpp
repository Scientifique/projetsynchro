#include<sys/ipc.h>
#include<sys/sem.h>
#include<sys/stat.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<cstdlib>
#include<ctime>

/*
struct sembuf {
    short sem_num;
    short sem_op;
    short sem_flg;
};
*/

/* structure décrivant les directions éventuelles des voitures ainsi
   que le nombre et le(s) numéro(s) de sémaphore(s) à réserver  */
struct passage {
    
    char *msg;
    int nb_reserv;
    int cases[3];
};

passage tpass[12];

int main(int argc, char *argv[])
{
    /* initialisation des passages possibles.
       les message comportant après la partie texte, une représentation
       visuelle du trajet effectué sur le carrefour */
    
    /* possibilités partant du bas */
    tpass[0].msg = "Coming from bottom to right\n....\n....\n..+A\n..D.";
    tpass[0].nb_reserv = 1;
    tpass[0].cases[0] = 2;
    
    tpass[1].msg = "Coming from bottom to top\n..A.\n..|.\n..|.\n..D.";
    tpass[1].nb_reserv = 2;
    tpass[1].cases[0] = 2;
    tpass[1].cases[1] = 1;
    
    tpass[2].msg = "Coming from bottom to left\n....\nA-+.\n..|.\n..D.";
    tpass[2].nb_reserv = 3;
    tpass[2].cases[0] = 2;
    tpass[2].cases[1] = 1;
    tpass[2].cases[2] = 0;
    
    /* possibilités partant de la droite */
    tpass[3].msg = "Coming from right to top\n..A.\n..+D\n....\n....";
    tpass[3].nb_reserv = 1;
    tpass[3].cases[0] = 1;
    
    tpass[4].msg = "Coming from right to left\n....\nA--D\n....\n....";
    tpass[4].nb_reserv = 2;
    tpass[4].cases[0] = 1;
    tpass[4].cases[1] = 0;
    
    tpass[5].msg = "Coming from right to bottom\n....\n.+-D\n.|..\n.A..";
    tpass[5].nb_reserv = 3;
    tpass[5].cases[0] = 1;
    tpass[5].cases[1] = 0;
    tpass[5].cases[2] = 3;
    
    /* possibilités partant du haut */
    tpass[6].msg = "Coming from top to left\n.D..\nA+..\n....\n....";
    tpass[6].nb_reserv = 1;
    tpass[6].cases[0] = 0;
    
    tpass[7].msg = "Coming from top to bottom\n.D..\n.|..\n.|..\n.A..";
    tpass[7].nb_reserv = 2;
    tpass[7].cases[0] = 0;
    tpass[7].cases[1] = 3;
    
    tpass[8].msg = "Coming from top to right\n.D..\n.|..\n.+-A\n....";
    tpass[8].nb_reserv = 3;
    tpass[8].cases[0] = 0;
    tpass[8].cases[1] = 3;
    tpass[8].cases[2] = 2;
    
    /* possibilités partant de la gauche */
    tpass[9].msg = "Coming from left to bottom\n....\n....\nD+..\n.A..";
    tpass[9].nb_reserv = 1;
    tpass[9].cases[0] = 3;
    
    tpass[10].msg = "Coming from left to right\n....\n....\nD--A\n....";
    tpass[10].nb_reserv = 2;
    tpass[10].cases[0] = 3;
    tpass[10].cases[1] = 2;
    
    tpass[11].msg = "Coming from left to top\n..A.\n..|.\nD-+.\n....";
    tpass[11].nb_reserv = 3;
    tpass[11].cases[0] = 3;
    tpass[11].cases[1] = 2;
    tpass[11].cases[2] = 1;
    
    srand(time(0));
    
    int rep;
    /* retour de P et de V */
    int mutex;
    /* identificateur de sémaphore */
    key_t sema = 404;
    /* nombre de sémaphores */
    int nb_sema = 4;
    /* CLE_IPC de mutex */
    
    /* on récupère l'id des 4 sémaphores du carrefour si elles existent */
    mutex=semget(sema, nb_sema, 0);
    
    if (mutex == -1) { perror("SEMGET"); exit(2); }
    
    printf("--> acquisition de 4 mutex pour PID=%d\n",getpid());
    
    printf("--> Valeur 1ier sema=%d pour PID=%d\n",
    semctl(mutex,0,GETVAL),getpid());
    
    printf("--> Valeur 2iem sema=%d pour PID=%d\n",
    semctl(mutex,1,GETVAL),getpid());
    
    printf("--> Valeur 3iem sema=%d pour PID=%d\n",
    semctl(mutex,2,GETVAL),getpid());
    
    printf("--> Valeur 4iem sema=%d pour PID=%d\n\n",
    semctl(mutex,3,GETVAL),getpid());
    
    int iter = 0;
    
    /* nombre d'itération (équivaut au nombre total de voitures qui seront
       simulées par une instance du programme) */
    int nbiter = 1000;
    
    while(iter < nbiter){
    
        printf("--> iteration %d\n", iter);
        
        /* On simule le passage d'une voiture dans une direction au hasard */
        int r = rand() % 12;
        int n = tpass[r].nb_reserv;
        
        struct sembuf tab_sema[n]; /* struct pour n sémaphores */
        
        for(int i=0; i<n; i++){
         
            /* on prépare les opération P pour les sémaphores qui seront
               occupées lors du passage de la voiture */
            tab_sema[i].sem_num=tpass[r].cases[i];
            tab_sema[i].sem_op=-1;
            tab_sema[i].sem_flg=0;
        }
        
        /* on applique l'(les) opérations(s) */
        rep=semop(mutex, tab_sema, n);
        
        /* quitte le programme en cas d'erreurs sur une opération */
        if (rep == -1) { 
            
            printf("--> Oups! P!\n");
        
            perror("P\n"); 
            exit(3); 
        }
        
        /* on décrit d'où vient la voiture et où elle va (directions)*/
        printf("--> %s \n", tpass[r].msg);

        /* attente le temps que la voiture passe */
        sleep(3);
        
        for(int i=0; i<n; i++){
            
            /* indication supplémentaire sur les sémaphores prises avant
               la libération */
            printf("--> Case %d prise \n", tpass[r].cases[i]);
         
            /* opération inverse (V), la voiture est passée et les sémaphores 
               seront donc libérées */
            tab_sema[i].sem_num=tpass[r].cases[i];
            tab_sema[i].sem_op=1;
            tab_sema[i].sem_flg=0;
        }
        
        /* on applique l'(les) opérations(s) */
        rep=semop(mutex, tab_sema, n);
        
        /* quitte le programme en cas d'erreurs sur une opération */
        if (rep == -1) {
        
            printf("--> Oups! V!\n");
            perror("V\n");
            exit(4);
        }
        
        printf("--> Case(s) liberee(s) \n\n");
        
        /* l'itération, et donc, le passage d'une voiture se termine */
        iter++;
        sleep(1);
    }
    
    printf("--> Termine \n");
    
    return 0;
}