#include <cstdlib>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main(int argc, char** argv) {

    int sema; /* sémaphore identification */
    key_t clef = 404; /* clef IPC */
	int nb_sema = 4; /* nombre de sémaphores à créer */
    int ret; /* code de retour de system call */
    
	/* récupération des sémaphores */
	sema = semget(clef, nb_sema, S_IRUSR|S_IWUSR);
	
	/* suppression des sémaphores */
	ret=semctl(sema, 0, IPC_RMID, 0);
	if (ret == -1) {
		printf("Les semaphores n'existent pas!\n");
		perror("SUPPR");
		exit(2);
	}
    
    printf("--> Semaphores supprimees\n");
    
    return 0;
}

